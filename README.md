# Cbio-docker-compose on demand

## Requirements
 * docker
 * docker-compose



## Deployment 

1. Clone the repository
2. Copy inject files to inject_data
3. Launch start.sh

Cbioportal should be available at 

<code>http:/localhost:8080</code>

Logs can be checked with

<code>docker-compose logs</code>
